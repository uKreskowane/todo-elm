import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import String
import Char


main =
  beginnerProgram { model = model, view = view, update = update }


-- MODEL
type alias Todo =
  { id : Int, body : String, completed : Bool }
type alias Model =
  { input : String, todos : List Todo, idTodo : Int }

model : Model
model =
  { input = ""
  , todos = []
  , idTodo = 0}


-- UPDATE
type Msg
  = AddTodo
  | RemoveAll
  | Input String
  | Remove Int
  | Complete Int


update : Msg -> Model -> Model
update msg model =
  case msg of
    AddTodo ->
      { model
      | todos = model.todos ++ [{ id = model.idTodo, body = model.input, completed = False }]
      , input = ""
      , idTodo = model.idTodo + 1 }

    RemoveAll ->
      { model | todos = [] }

    Input text ->
      { model | input = text }

    Remove id ->
      { model
      | todos = List.filter (\todo -> todo.id /= id) model.todos }

    Complete id ->
      { model
      | todos = List.map (\todo ->
          if todo.id == id then { todo | completed = not todo.completed } else todo ) model.todos }


-- VIEW
view : Model -> Html Msg
view model =
  Html.form [onSubmit AddTodo, class "container"]
    [ h1 [class "headline"] [text "ToDo Application written in Elm"]
    , input
        [ type_ "text"
        , placeholder "Twoje zadanie"
        , onInput Input
        , value model.input
        , class "task-input"
        , required True
        ] []
    , div [ class "tools" ]
        [ button [ type_ "submit", class "btn" ] [ text "Dodaj" ]
        , button [ type_ "button", class "btn", onClick RemoveAll ] [ text "Resetuj" ]
        , changeView model
        , button [ type_ "button", class "btn" ] [ text "Zmień" ]
        ]
    , ul [] ( List.map todoEl model.todos )
    , node "style" [type_ "text/css"] [text "@import url(./style.css)"]
    ]


todoEl : Todo -> Html Msg
todoEl todo =
  li [class (if todo.completed then "task task--completed" else "task")]
    [ button [ type_ "button", class "task__button task__button--check", onClick (Complete todo.id) ] [ text (String.fromChar (Char.fromCode 10003)) ]
    , span [] [ text todo.body ]
    , button [ type_ "button", class "task__button task__button--delete", onClick (Remove todo.id) ] [ text (String.fromChar (Char.fromCode 10007)) ]
    ]

changeView : Model -> Html Msg
changeView model =
  Html.form [class "change-view"]
  [ select [  ]
      [ option [ value "all" ] [ text "Wszystkie" ]
      , option [ value "completed" ] [ text "Wykonane" ]
      , option [ value "uncompleted" ] [ text "Niewykonane" ] ]
  ]

